package org.umss.sisii.minisis.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.List;
import java.util.ArrayList;
import org.umss.sisii.minisis.model.Score;
import org.umss.sisii.minisis.model.Task;
import org.umss.sisii.minisis.model.User;

/**
 *
 * @author PC_
 */
public class DBManager {

    private static final String GET_SCORE_BY_USER_ID
            = "select * from get_scores_by_user_id(%d);";
    private static final String VERIFY_USER
            = "select verify_user('%s', '%s')";
    private static DBManager manager = null;
    private static final Logger LOGGER = Logger.getLogger(DBManager.class.getName());
    private Connection connection = null;
    private ResultSet rs = null;
    private Statement s = null;
    private final String url;
    private final String password;
    private final String postgresUser;

    public static DBManager getInstance() {
        if (manager == null) {
            manager = new DBManager();
        }
        return manager;
    }

    private DBManager() {
        url = "jdbc:postgresql://localhost:5432/mini_sis_database";
        password = "postgres";
        postgresUser = "postgres";
        conectar();
    }

    private void conectar() {
        if (connection != null) {
            return;
        }
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, postgresUser, password);
            if (connection != null) {
                s = connection.createStatement();
                System.out.println("Conectando a Base de Datos...");
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Problemas de Conexion");
        }
    }

    public User checkUser(User user) {
        try {
            String query = String.format(VERIFY_USER, user.getUserName(), user.getUserPassword());
            rs = s.executeQuery(query);
            rs.next();
            user.setId(rs.getInt(1));
            System.out.println(user.getId() + user.getUserName());
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
        return user;
    }

    public List<Task> getAllTasks() {
        List<Task> tasks = new ArrayList<>();
        try {
            rs = s.executeQuery("SELECT task_name, task_description, start_date, end_date FROM \"task\";");
            while (rs.next()) {
                Task task = new Task();
                task.setTaskName(rs.getString("task_name"));
                task.setDescription(rs.getString("task_description"));
                task.setStartDate(rs.getString("start_date"));
                task.setEndDate(rs.getString("end_date"));
                tasks.add(task);
            }
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
        return tasks;
    }

    public List<Score> getScores(int userId) {
        List<Score> result = new ArrayList<>();
        Score current;
        try {
            rs = s.executeQuery(String.format(GET_SCORE_BY_USER_ID, userId));
            while (rs.next()) {
                current = new Score();
                current.setTask(rs.getString("task_name"));
                current.setScore(Integer.valueOf(rs.getString("score")));
                result.add(current);
            }
        } catch (SQLException ex) {
        }
        return result;
    }

}
