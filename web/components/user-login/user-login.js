(function () {
    'use strict';
    angular.module('app.userLogin', [])
            .controller('UserLoginController', UserLoginController);

    UserLoginController.$inject = ['$location', 'UserService'];

    function UserLoginController($location, UserService) {
        var vm = this;
        vm.userName;
        vm.userPassword;
        vm.userId;

        vm.verifyUser = function () {
            var user = {
                userName: vm.userName,
                userPassword: vm.userPassword
            };
            UserService.verifyUser(user, onSuccess, onError);
        };

        function onSuccess(data) {
            console.log("User Exist");
            console.log("... " + data.data.userName);
            console.log("... " + data.data.userPassword);
            console.log("... " + data.data.id);
            console.log(data);
            vm.userId = data.data.id;
            console.log(vm.userId);
            if (vm.userId > 0) {
                $location.path('taskList');
                UserService.userId = vm.userId;
                console.log(UserService.userId);
            } else {
                alert('No estas registrado');
            }
        }
        ;

        function onError() {
            console.log("User not exist!");
        }
        ;
    }
    ;
})();