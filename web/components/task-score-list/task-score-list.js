(function () {
    'use strict';

    angular.module('app.taskScoreList', [])
           .controller('TaskScoreListController', TaskScoreListController);

    TaskScoreListController.$inject = ['TaskService', 'UserService'];
    function TaskScoreListController(TaskService, UserService) {
        var vm = this;
        vm.scores = [];
        vm.userId = UserService.userId;
        var user = {
            id: vm.userId
        };
        TaskService.getScores(user, onSuccess, onError);

        function onSuccess(data) {
            vm.scores = data.data;
        }

        function onError() {
        }
    }
})();